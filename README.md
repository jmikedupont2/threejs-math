# sketch-threejs-v2

[![npm version][npmv-image]][npmv-url]
[![build status][build-image]][build-url]
[![coverage status][codecov-image]][codecov-url]
[![npm downloads][npmd-image]][npmd-url]

> Three Js sketches

## Basic Usage

```jsx
import React from 'react'
import { render } from 'react-dom'

render(, document.getElementById('root'))
```

## Live Examples

- [Basic Usage](https://codesandbox.io/)
- [API Example](https://codesandbox.io/)
- [UMD Build (Development)](https://codesandbox.io/)
- [UMD Build (Production)](https://codesandbox.io/)

## API

**Props**

- `foo` - Something something.
- `bar` - _Optional_ Something something. Defaults to `null`.

**Example**

```jsx

```

## Installation

```
$ npm install sketch-threejs-v2 --save
```

There are also UMD builds available via [unpkg](https://unpkg.com/):

- https://unpkg.com/sketch-threejs-v2/dist/sketch-threejs-v2.umd.development.js
- https://unpkg.com/sketch-threejs-v2/dist/sketch-threejs-v2.umd.production.js

For the non-minified development version, make sure you have already included:

- [`React`](https://unpkg.com/react/umd/react.development.js)
- [`ReactDOM`](https://unpkg.com/react-dom/umd/react-dom.development.js)
- [`PropTypes`](https://unpkg.com/prop-types/prop-types.js)

For the minified production version, make sure you have already included:

- [`React`](https://unpkg.com/react/umd/react.production.min.js)
- [`ReactDOM`](https://unpkg.com/react-dom/umd/react-dom.production.min.js)

## License

MIT

[build-image]: https://img.shields.io/github/workflow/status/jmikedupont2/sketch-threejs-v2/CI?style=flat-square
[build-url]: https://github.com/jmikedupont2/sketch-threejs-v2/actions?query=workflow%3ACI
[codecov-image]: https://img.shields.io/codecov/c/github/jmikedupont2/sketch-threejs-v2.svg?style=flat-square
[codecov-url]: https://codecov.io/gh/jmikedupont2/sketch-threejs-v2
[npmv-image]: https://img.shields.io/npm/v/sketch-threejs-v2.svg?style=flat-square
[npmv-url]: https://www.npmjs.com/package/sketch-threejs-v2
[npmd-image]: https://img.shields.io/npm/dm/sketch-threejs-v2.svg?style=flat-square
[npmd-url]: https://www.npmjs.com/package/sketch-threejs-v2
