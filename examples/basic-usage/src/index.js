import C from 'sketch-threejs-v2'
import React from 'react'
import ReactDOM from 'react-dom'

ReactDOM.render(<C />, document.getElementById('root'))
